<?php
date_default_timezone_set('Etc/UTC');

if($_POST['formSubmit'] == "Submit")
{
    $errorMessage = "";
    
	if(empty($_POST['formName']))
	{
		$errorMessage .= "<li>Has olvidado introducir un nombre!</li>";
	}
	if(empty($_POST['formEmail']))
	{
		$errorMessage .= "<li>Has olvidado introducir una dirección de correo electrónico!</li>";
	}
	
	$varName = $_POST['formName'];
	$varEmail = $_POST['formEmail'];

	if(empty($errorMessage)) 
	{
		$fs = fopen("productcert.csv","a");
		fwrite($fs,$varName . ", " . $varEmail . "\n");
		fclose($fs);
		$msg = $varName . '(' . $varEmail . ')' . ' has completed Product Certification and need their 5 SolleRewards Points';
      mail ('ledhead0501@gmail.com', 'Product Certification', $msg);
		header("Location: /solleprocertified/thankyou1es.html");
		exit();
	}
}
?>
<!DOCTYPE = html>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/assets/js/vendor/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/a0d03b02a0.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="/solleprocertified/js/jquery.modal.min.js"></script>
<html>
<html lang = "es">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" >
<link href="https://fonts.googleapis.com/css?family=Oswald|Trocchi|Lobster|Montserrat|Merriweather|Shrikhand|Raleway|Montserrat|Creepster|Ribeye+Marrow|Luckiest+Guy" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,900,700italic,900italic,500,500italic, 300' rel='stylesheet' type='text/css'>
<link href="/solleprocertified/css/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="/solleprocertified/css/kustom.css" type="text/css" rel="stylesheet" />
<style>
  #top_border {
  list-style-type: none;
  margin: 0;
  padding : 0;
  overflow: hidden;
  position: fixed;
  background-color: #3d3d3d;
  border-radius: 0px;
  width: 100%;
  height: 50px;
  z-index: +1;
  margin-top: -70px;
  }
  
  
  li{
    display: block;
    color: white;
    float: right;
    padding: 15px 17px;
    font-family: Roboto;
    font-weight: bold;
    text-align: center;
    
  }
  li:hover{
      background-color: #c1d72e;
  }
  a{
      color: white;
  }
  a:hover{
      color: white;
      text-decoration: none;
  }
  a:click{
      color: white;
      text-decoration: none;
  }
 #mid_border{
 list-style-type:none;
 margin-top: -20px;
 padding: 0;
 overflow: hidden;
 position: fixed;
 background-color: #c1d72e;
 border-radius: 0px;
 width: 100%;
 height: 5px;
 box-shadow: 0px 3px 2px 0px #3b3b3b;
 }
 
 #img_logo{
  margin-left: 30px;
  margin-right: auto;
  margin-top: 5px;
  }
  
.bannerbar{
    display: block
    position: absolute;
    border: 0px;
    border-style: solid;
    border-radius: 5px;
    padding-top: 20px;
    padding-bottom: 30px;
    margin: 0 auto;
    margin-top: 0px;
    background: #c1d72e;
    max-width:99%;
    box-shadow: 10px 5px 5px #3d3d3d;
}

 
 body{
 background: radial-gradient(#929292, #424242); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
 }
 
 .introslides{
     display: block;
     margin-left: auto;
     margin-right: auto;
     margin-top: 10px;
     margin-bottom: 20px;
     z-index: -1;
 }
 
 h1{
 font-family: Roboto;
 font-weight: bold;
 color: white;}
 
 .bodytext{
     color: white;
     font-family: Roboto;
     font-size: 25px;
     font-weight: bold;
     text-shadow: 1px 1px 1px #3d3d3d;
 }
 
 #productcertbutton{
     display: block;
     position: relative;
     font-family: Roboto;
     font-weight: bold;
     font-size: 20px;
     margin-left: auto;
     margin-right: auto;
     margin-bottom: 20px;
     margin-top: 30px;
     background-color:rgba(0,0,0,0.0);
     color: white;
     border-style: solid;
     border-color: white;
     border-radius: 10px;
 }
 
 #productcertbutton:hover{
     background-color: #c1d72e;
     color: white;
 }
 #mentorcertbutton{
     display: block;
     float: right;
     font-family: Roboto;
     font-weight: bold;
     font-size: 20px;
     margin-right: 20%;
     margin-bottom: 20px;
     background-color:rgba(0,0,0,0.0);
     color: white;
     border-style: solid;
     border-color: white;
     border-radius: 10px;
 }
 
 #mentorcertbutton:hover{
     background-color: #009ec5;
     color: white;
 }
 
 footer{
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  position: relative;
  background-color: #3d3d3d;
  border-radius: 0px;
  width: 100%;
  height: 515px;}
  
 #certified_image{
 display: block;
 margin-left: auto;
 margin-right: auto;
 margin-top: -170px;
 margin-bottom: -160px;
 height: 850px;
 width:  650px;}
 
 
 #footer-leaf{
 display: block;
 position: absolute;
 height: 100%;
 float: left;}
 
 .social_button{
 display: block;
 margin-left: auto;
 margin-right: auto;}

.disclaimer-button:hover{
background-color:#c1d72e;
color: white; }

.social-buttons{
background-color: #3d3d3d;
border-style: solid;
border-radius: 20px;
color: #d9d9d9;
height: 40px;
width: 40px;
margin-left: 15px;}

.social-buttons:hover{
background-color: #c1d72e;
color: white;}

#facebook-button{
margin-bottom: 20px;
margin-top: -20px;
margin-left: 10px;}

.modal-header{
background-color:#3d3d3d;
border-bottom-color: #3d3d3d;
}

.modal-body{
background-color: #3d3d3d;
border-color: #3d3d3d;
padding-bottom: 1px;
}
.modal-title{
font-family: Roboto;
color: white;
text-align: center;}

.modal-text{
font-family: Roboto;
color: white;
text-align: left;
}
.modal-footer{
background-color:#3d3d3d;
border-top-color: #3d3d3d;
margin-top: 1px;
}
.modal-content{
position: relative;
background-color: #3d3d3d;
z-index: +2;
}
.modal-dialog{
background-color: #3d3d3d;
}
.btn{
background-color: #3d3d3d;
color: white;}

.btn:hover{
background-color: #c1d72e;
color: white;}

#pdftext{
    color: white;
    margin-top: 20px;
    text-align: center;
}

embed{
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 30px;
}

.winner-input{
    display: block;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    color: #3d3d3d;
}

#cert-text{
text-align: center;
font-size: 25px;
}

#winner-button{
    display: block;
    float: center;
    color: white;
    border-color: white;
    border-style: solid;
    border-radius: 15px;
    background-color: #c1d72e;
    margin-left: auto;
    margin-right: auto;
}

.pdfbutton{
	 display: block;
	 width: 100px;
    margin-top: 20px;
    margin-bottom: 30px;
    margin-left: auto;
    margin-right: auto;
    font-size: 15px;
    padding-left: 3px;
    padding-right: 3px;
    font-style: bold;
    border-style: solid;
    border-radius: 10px;
    text-align: center;
}

.pdfbutton:hover{
    background-color: #c1d72e;
}


</style>
<head>
<title>Certificación del producto</title>
<div>
  <ul id="top_border">
  <a href="https://www.sollenaturals.com/home.php">
  <img id="img_logo" src="/solleprocertified/images/logo.jpg" alt="Solle Naturals Logo" height="40px" width="120px<br>"> </a>
  <li> <a href="/solleprocertified/productcert.php">English Version</a></li>
  <li> <a href="/solleprocertified/presenters-es.html">Presentadores</a></li>
  <li> <a onclick="document.getElementById('top_border').setAttribute('style', 'position: absolute; z-index: initial;');" data-toggle="modal" href="#assmodal">Evaluación</a>
  <div id="assmodal" class="modal fade" role="dialog">
     <div class="modal-dialog">
 	
 	<!-- Modal Content -->
 	
 	<div class="modal-content">
 		<div class="modal-header">
 		<button onclick="document.getElementById('top_border').setAttribute('style', 'position: fixed; z-index: +1;');" type="button" class="close" data-dismiss="modal">&times;</button>
 		<h4 class="modal-title">¿Seguro de continuar?</h4>
 		</div>
 		<div class="modal-body">
 		<p class="modal-text">¿Ha completado el entrenamiento de Mentor y de Productos? <br> <br>
         Si no lo ha hecho, por favor complételo antes de hacer la evaluación. <br> <br>
 		</p>
 		</div>
 		<div class="modal-footer">
         <button class ="btn btn-default"onclick="window.location.href='https://www.sollenaturals.com/solleprocertified/quiz-es.php'">Continuar</button>
 		<button onclick="document.getElementById('top_border').setAttribute('style', 'position: fixed; z-index: +1;');" type="button" class="btn btn-default" data-dismiss="modal">Cerca</button>
 		</div>
 	</div>
 </div>
</div> </li>
  <li> <a href="https://www.sollenaturals.com/solleprocertified/mentorcert-es.html" >Entrenamiento Mentores </a></li>
  <li> <a href="https://www.sollenaturals.com/solleprocertified/productcert-es.php">Entrenamiento sobre Productos </a></li>
  <li> <a href="https://www.sollenaturals.com/solleprocertified/home-es.html">Inicio</a></li>
  </ul>
  </div>
<div> <ul id="mid_border">
</ul>
</head> <br><br><br>
<body>
<br> <p class=" bannerbar bodytext" align="center">¡Bienvenido a la Certificación de Productos!<br> Paso 1- Filosofía y Compromiso de Calidad Solle
</p> <br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/product_intro_spanish.mp4"></video>
<br> <p class=" bannerbar bodytext" align="center">Paso 2 - El poder de los adaptógenos y las 4 categorías de productos Solle   
</p> <br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/spanish_intro_2.mp4"></video>
<br> <p class =" bannerbar bodytext" align="center">Paso 3 - Presentando la Categoría Balance
</p> <br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/balance_intro_spanish.mp4"></video>
<br> <p class =" bannerbar bodytext" align="center">Paso 4 - Revisión profunda de los productos en la categoría Balance</p> <br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/balance_spanish.mp4"></video>
<br> <p class= "bannerbar bodytext" align="center">Paso 5 - Presentando la Categoría Lift</p><br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/lift_spanish_intro.mp4"></video>
<br> <p class= "bannerbar bodytext" align="center">Paso 6 - Revisión profunda de los productos en la categoría Lift </p><br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/lift_spanish.mp4"></video>
<br> <p class="bannerbar bodytext" align="center">Paso 7 - Presentando la Categoría Clarify</p> <br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/clarify_spanish_intro.mp4"></video>
<br> <p class= "bannerbar bodytext" align="center">Paso 8 - Revisión profunda de los productos en la categoría Clarify</p><br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/clarify_spanish.mp4"></video>
<br> <p class= "bannerbar bodytext" align="center">Paso 9 - Presentando la Categoría Calm</p><br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/calm_spanish_intro.mp4"></video>
<br> <p class="bannerbar bodytext" align="center">Paso 10 - Revisión profunda de los productos en la categoría Calm</p><br>
<video class="introslides" width="700" height="500" controls><source src="/solleprocertified/videos/calm_spanish.mp4"></video>
 <br> <div class="bannerbar"> <p class="bodytext" id="cert-text" >¡Felicitaciones! Usted ha completado el Entrenamiento y la Certificación SollePro<br>
         Por favor escriba su nombre y su email para recibir 5 SolleRewards<br></p>
         <form name="myForm" action="productcert-es.php" method="post" class="winner-input"> Nombre: <input style="margin-bottom: 10px;"id="input-text" type="text" name="formName" value="<?=$varName;?>" required ><br>
         Email: <input style="margin-left: 15px;" id="input-text" type="text" name="formEmail" value="<?=$varEmail;?>" required><br><br>
         <button id="winner-button" type="submit" name="formSubmit" value="Submit">Enviar</button>
         </form> </div> 
         <br> <br>
 




</body>
 <footer class="container-fluid"><img id="footer-leaf" src="/solleprocertified/images/footer_leaf.png"> 
 <div class="footer_text"> <div id="center-buttons"><button class="fa fa-facebook fa-2x social-buttons" id="facebook-button"  onclick="window.open('https://www.facebook.com/SolleNaturals/','_blank');";></button>
 <button class="fa fa-instagram fa-2x social-buttons" onclick="window.open('https://www.instagram.com/sollenaturalstm/','_blank');";> </button> 
 <button class="fa fa-twitter fa-2x social-buttons" onclick="window.open('https://twitter.com/sollenaturals1','_blank');";></button> 
 <button class="fa fa-youtube fa-2x social-buttons" onclick="window.open('https://www.youtube.com/user/sollenaturals','_blank');";> </button> </div>
 <p align="center"> 260 S. 2500 W.<br> Suite 102, Pleasant Grove, Utah 84062 <br> <br>
 Toll Free #:888-787-0665 <br> Email: info@sollenaturals.com </p> <br> <br>
 <img id="footer-logo" src="/solleprocertified/images/solle_logo_footer.png"> <br> <br> 
 <p align="center" id="copyright-text"> &copy Copyright Solle Naturals 2017. Todos los derechos reservados.</p> <br> <br>
 
 <!-- Modal Bootstrap testing -->
 
 <button class="disclaimer-button" data-toggle="modal" data-target="#mymodal">Nota Legal</button>
 <div id="mymodal" class="modal fade" role"dialog">
 	<div class="modal-dialog">
 	
 	<!-- Modal Content -->
 	
 	<div class="modal-content">
 		<div class="modal-header">
 		<button type="button" class="close" data-dismiss="modal">&times;</button>
 		<h4 class="modal-title">Nota Legal</h4>
 		</div>
 		<div class="modal-body">
 		<p class="modal-text">The content of this website is intended for education purposes only. <br>
         It is not intended to be a substitute for professional healthcare advice, diagnosis or treatment. <br>
         We encourage you to consult your healthcare professional if you have concerns about your physical or emotional well-being.</p>
 		</div>
 		<div class="modal-footer">
 		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 		</div>
 	</div>
 </div>
</div>
</footer>